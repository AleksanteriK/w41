import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import IntervalClock from './Interval';

function App() {

  return (
    <IntervalClock></IntervalClock>
  );
}

export default App;
