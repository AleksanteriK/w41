import { useState, useEffect } from "react";

export default function IntervalClock () {
    var new_time;
    const [times, setTime] = useState([]);

    function formatUnixTimestamp(timestamp) {
        const date = new Date(timestamp);
        const hours = date.getHours().toString().padStart(2, "0");
        const minutes = date.getMinutes().toString().padStart(2, "0");
        const seconds = date.getSeconds().toString().padStart(2, "0");
        const milliseconds = date.getMilliseconds().toString().padStart(3, "0");
        const offset = date.getTimezoneOffset() / 60;
    
        return `${hours}:${minutes}:${seconds}-${milliseconds}`;
    }

    function setNewTime() {
        new_time = Date.now();
        setTime((prevTimes) => [...prevTimes, formatUnixTimestamp(new_time)]);
    }

    useEffect (() => {
        const abortController = new AbortController();
        console.log("run");

        return () => abortController.abort(); 
    }, [times])

    return (
        <div>
            <button onClick = {setNewTime} style = {{fontSize: "36px"}}>Interval Time</button>
            <table>
                <tbody id = "list">
                    {times.map((item, index) => (
                        <tr key={index}>
                        <td>{item}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
 }